#! /bin/sh

set -e

ocamlfind ocamlc -syntax camlp4o -package js_of_ocaml,js_of_ocaml.syntax -linkpkg webpdb.ml -o webpdb.byte
js_of_ocaml webpdb.byte -o webpdb.js

cat  > webpdb.html <<EOF
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>PDB Demo Web-Viewer</title>

<style type="text/css">
<!--
body {background-color:black; color:white}
canvas {float:left;}
-->
</style>
<script id="vertex-shader" type="x-shader/x-vertex">
  attribute vec3 a_position;
  attribute vec3 a_normal;
  attribute vec3 a_color;

  uniform mat4 u_proj;

  varying mediump vec3 v_position;
  varying mediump vec3 v_normal;
  varying mediump vec3 v_color;

  void main() {
    vec4 pos = u_proj * vec4(a_position,1);
    vec4 norm = u_proj * vec4(a_normal,1);
    v_position = pos.xyz;
    v_normal = norm.xyz;
    v_color = a_color;
    gl_Position = pos;
  }
</script>
<script id="fragment-shader" type="x-shader/x-fragment">
  precision mediump float;
  varying vec3 v_position;
  varying vec3 v_normal;
  varying vec3 v_color;

  uniform vec3 u_lightPos;
  uniform vec3 u_ambientLight;

  void main() {
    vec3 lightDirection = normalize(u_lightPos - v_position);
    float lighting = max(dot(normalize(v_normal), lightDirection), 0.);
    gl_FragColor = vec4( v_color * lighting + u_ambientLight, 1);
  }
</script>
    <script >
EOF
cat webpdb.js >> webpdb.html

cat >> webpdb.html <<EOF
</script>
  </head>
  <body>
    <canvas id="canvas" height="800" width="800"></canvas>
    <div><span id="fps"></span> frames per second</div>
    <div id="interaction" />
  </body>
</html>
EOF
