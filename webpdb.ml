(* Contents of the Renderer module are taken from the
 * “Js_of_ocaml example” * http://www.ocsigen.org/js_of_ocaml/
 * Copyright (C) 2012 Pierre Chambart
 *
 * Everything else is:
 * Copyright (C) 2013 Sebastien Mondet <seb -at- mondet.org>
 *
 * The original license was LGPL but Piere Chambart agreed that BSD-like would
 * be more useful for the community:
 * “Fais ce que tu veux avec, aucun problème, change de license comme tu
 * veux. Heureux que ça puisse servir.”
 * So, here is the ISC License:
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 *  copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 *
 *)
open Lwt
open Js
module List = struct
  include ListLabels
  let map l ~f = rev (rev_map l ~f)
end
module Array = ArrayLabels
let (|>) x f = f x

let error f =
  Printf.ksprintf (fun s -> Firebug.console##error (Js.string s); failwith s) f
let debug f =
  Printf.ksprintf (fun s -> Firebug.console##log(Js.string s)) f
let alert f =
  Printf.ksprintf (fun s -> Dom_html.window##alert(Js.string s); failwith s) f

let check_error gl =
  if gl##getError() <> gl##_NO_ERROR
  then error "WebGL error"

module Data = struct

  let sphere_vertices = [|
    0.723600; -0.525720; -0.447215;
    0.262869; -0.809012; -0.525738;
    0.425323; -0.309011; -0.850654;
    -0.162456; -0.499995; -0.850654;
    0.425323; -0.309011; -0.850654;
    0.262869; -0.809012; -0.525738;
    0.262869; -0.809012; -0.525738;
    -0.276385; -0.850640; -0.447215;
    -0.162456; -0.499995; -0.850654;
    0.425323; -0.309011; -0.850654;
    -0.162456; -0.499995; -0.850654;
    0.000000; 0.000000; -1.000000;
    0.425323; -0.309011; -0.850654;
    0.850648; 0.000000; -0.525736;
    0.723600; -0.525720; -0.447215;
    0.850648; 0.000000; -0.525736;
    0.425323; -0.309011; -0.850654;
    0.425323; 0.309011; -0.850654;
    0.425323; 0.309011; -0.850654;
    0.723600; 0.525720; -0.447215;
    0.850648; 0.000000; -0.525736;
    0.425323; 0.309011; -0.850654;
    0.425323; -0.309011; -0.850654;
    0.000000; 0.000000; -1.000000;
    -0.276385; -0.850640; -0.447215;
    -0.688189; -0.499997; -0.525736;
    -0.162456; -0.499995; -0.850654;
    -0.525730; 0.000000; -0.850652;
    -0.162456; -0.499995; -0.850654;
    -0.688189; -0.499997; -0.525736;
    -0.688189; -0.499997; -0.525736;
    -0.894425; 0.000000; -0.447215;
    -0.525730; 0.000000; -0.850652;
    -0.162456; -0.499995; -0.850654;
    -0.525730; 0.000000; -0.850652;
    0.000000; 0.000000; -1.000000;
    -0.894425; 0.000000; -0.447215;
    -0.688189; 0.499997; -0.525736;
    -0.525730; 0.000000; -0.850652;
    -0.162456; 0.499995; -0.850654;
    -0.525730; 0.000000; -0.850652;
    -0.688189; 0.499997; -0.525736;
    -0.688189; 0.499997; -0.525736;
    -0.276385; 0.850640; -0.447215;
    -0.162456; 0.499995; -0.850654;
    -0.525730; 0.000000; -0.850652;
    -0.162456; 0.499995; -0.850654;
    0.000000; 0.000000; -1.000000;
    -0.276385; 0.850640; -0.447215;
    0.262869; 0.809012; -0.525738;
    -0.162456; 0.499995; -0.850654;
    0.425323; 0.309011; -0.850654;
    -0.162456; 0.499995; -0.850654;
    0.262869; 0.809012; -0.525738;
    0.262869; 0.809012; -0.525738;
    0.723600; 0.525720; -0.447215;
    0.425323; 0.309011; -0.850654;
    -0.162456; 0.499995; -0.850654;
    0.425323; 0.309011; -0.850654;
    0.000000; 0.000000; -1.000000;
    0.850648; 0.000000; -0.525736;
    0.951058; -0.309013; 0.000000;
    0.723600; -0.525720; -0.447215;
    0.951058; -0.309013; 0.000000;
    0.850648; 0.000000; -0.525736;
    0.951058; 0.309013; 0.000000;
    0.951058; 0.309013; 0.000000;
    0.894425; 0.000000; 0.447215;
    0.951058; -0.309013; 0.000000;
    0.723600; 0.525720; -0.447215;
    0.951058; 0.309013; 0.000000;
    0.850648; 0.000000; -0.525736;
    0.262869; -0.809012; -0.525738;
    0.000000; -1.000000; 0.000000;
    -0.276385; -0.850640; -0.447215;
    0.000000; -1.000000; 0.000000;
    0.262869; -0.809012; -0.525738;
    0.587786; -0.809017; 0.000000;
    0.587786; -0.809017; 0.000000;
    0.276385; -0.850640; 0.447215;
    0.000000; -1.000000; 0.000000;
    0.723600; -0.525720; -0.447215;
    0.587786; -0.809017; 0.000000;
    0.262869; -0.809012; -0.525738;
    -0.688189; -0.499997; -0.525736;
    -0.951058; -0.309013; 0.000000;
    -0.894425; 0.000000; -0.447215;
    -0.951058; -0.309013; 0.000000;
    -0.688189; -0.499997; -0.525736;
    -0.587786; -0.809017; 0.000000;
    -0.587786; -0.809017; 0.000000;
    -0.723600; -0.525720; 0.447215;
    -0.951058; -0.309013; 0.000000;
    -0.276385; -0.850640; -0.447215;
    -0.587786; -0.809017; 0.000000;
    -0.688189; -0.499997; -0.525736;
    -0.688189; 0.499997; -0.525736;
    -0.587786; 0.809017; 0.000000;
    -0.276385; 0.850640; -0.447215;
    -0.587786; 0.809017; 0.000000;
    -0.688189; 0.499997; -0.525736;
    -0.951058; 0.309013; 0.000000;
    -0.951058; 0.309013; 0.000000;
    -0.723600; 0.525720; 0.447215;
    -0.587786; 0.809017; 0.000000;
    -0.894425; 0.000000; -0.447215;
    -0.951058; 0.309013; 0.000000;
    -0.688189; 0.499997; -0.525736;
    0.262869; 0.809012; -0.525738;
    0.587786; 0.809017; 0.000000;
    0.723600; 0.525720; -0.447215;
    0.587786; 0.809017; 0.000000;
    0.262869; 0.809012; -0.525738;
    0.000000; 1.000000; 0.000000;
    0.000000; 1.000000; 0.000000;
    0.276385; 0.850640; 0.447215;
    0.587786; 0.809017; 0.000000;
    -0.276385; 0.850640; -0.447215;
    0.000000; 1.000000; 0.000000;
    0.262869; 0.809012; -0.525738;
    0.894425; 0.000000; 0.447215;
    0.688189; -0.499997; 0.525736;
    0.951058; -0.309013; 0.000000;
    0.587786; -0.809017; 0.000000;
    0.951058; -0.309013; 0.000000;
    0.688189; -0.499997; 0.525736;
    0.688189; -0.499997; 0.525736;
    0.276385; -0.850640; 0.447215;
    0.587786; -0.809017; 0.000000;
    0.951058; -0.309013; 0.000000;
    0.587786; -0.809017; 0.000000;
    0.723600; -0.525720; -0.447215;
    0.276385; -0.850640; 0.447215;
    -0.262869; -0.809012; 0.525738;
    0.000000; -1.000000; 0.000000;
    -0.587786; -0.809017; 0.000000;
    0.000000; -1.000000; 0.000000;
    -0.262869; -0.809012; 0.525738;
    -0.262869; -0.809012; 0.525738;
    -0.723600; -0.525720; 0.447215;
    -0.587786; -0.809017; 0.000000;
    0.000000; -1.000000; 0.000000;
    -0.587786; -0.809017; 0.000000;
    -0.276385; -0.850640; -0.447215;
    -0.723600; -0.525720; 0.447215;
    -0.850648; 0.000000; 0.525736;
    -0.951058; -0.309013; 0.000000;
    -0.951058; 0.309013; 0.000000;
    -0.951058; -0.309013; 0.000000;
    -0.850648; 0.000000; 0.525736;
    -0.850648; 0.000000; 0.525736;
    -0.723600; 0.525720; 0.447215;
    -0.951058; 0.309013; 0.000000;
    -0.951058; -0.309013; 0.000000;
    -0.951058; 0.309013; 0.000000;
    -0.894425; 0.000000; -0.447215;
    -0.723600; 0.525720; 0.447215;
    -0.262869; 0.809012; 0.525738;
    -0.587786; 0.809017; 0.000000;
    0.000000; 1.000000; 0.000000;
    -0.587786; 0.809017; 0.000000;
    -0.262869; 0.809012; 0.525738;
    -0.262869; 0.809012; 0.525738;
    0.276385; 0.850640; 0.447215;
    0.000000; 1.000000; 0.000000;
    -0.587786; 0.809017; 0.000000;
    0.000000; 1.000000; 0.000000;
    -0.276385; 0.850640; -0.447215;
    0.276385; 0.850640; 0.447215;
    0.688189; 0.499997; 0.525736;
    0.587786; 0.809017; 0.000000;
    0.951058; 0.309013; 0.000000;
    0.587786; 0.809017; 0.000000;
    0.688189; 0.499997; 0.525736;
    0.688189; 0.499997; 0.525736;
    0.894425; 0.000000; 0.447215;
    0.951058; 0.309013; 0.000000;
    0.587786; 0.809017; 0.000000;
    0.951058; 0.309013; 0.000000;
    0.723600; 0.525720; -0.447215;
    0.688189; -0.499997; 0.525736;
    0.162456; -0.499995; 0.850654;
    0.276385; -0.850640; 0.447215;
    0.162456; -0.499995; 0.850654;
    0.688189; -0.499997; 0.525736;
    0.525730; 0.000000; 0.850652;
    0.525730; 0.000000; 0.850652;
    0.000000; 0.000000; 1.000000;
    0.162456; -0.499995; 0.850654;
    0.894425; 0.000000; 0.447215;
    0.525730; 0.000000; 0.850652;
    0.688189; -0.499997; 0.525736;
    -0.262869; -0.809012; 0.525738;
    -0.425323; -0.309011; 0.850654;
    -0.723600; -0.525720; 0.447215;
    -0.425323; -0.309011; 0.850654;
    -0.262869; -0.809012; 0.525738;
    0.162456; -0.499995; 0.850654;
    0.162456; -0.499995; 0.850654;
    0.000000; 0.000000; 1.000000;
    -0.425323; -0.309011; 0.850654;
    0.276385; -0.850640; 0.447215;
    0.162456; -0.499995; 0.850654;
    -0.262869; -0.809012; 0.525738;
    -0.850648; 0.000000; 0.525736;
    -0.425323; 0.309011; 0.850654;
    -0.723600; 0.525720; 0.447215;
    -0.425323; 0.309011; 0.850654;
    -0.850648; 0.000000; 0.525736;
    -0.425323; -0.309011; 0.850654;
    -0.425323; -0.309011; 0.850654;
    0.000000; 0.000000; 1.000000;
    -0.425323; 0.309011; 0.850654;
    -0.723600; -0.525720; 0.447215;
    -0.425323; -0.309011; 0.850654;
    -0.850648; 0.000000; 0.525736;
    -0.262869; 0.809012; 0.525738;
    0.162456; 0.499995; 0.850654;
    0.276385; 0.850640; 0.447215;
    0.162456; 0.499995; 0.850654;
    -0.262869; 0.809012; 0.525738;
    -0.425323; 0.309011; 0.850654;
    -0.425323; 0.309011; 0.850654;
    0.000000; 0.000000; 1.000000;
    0.162456; 0.499995; 0.850654;
    -0.723600; 0.525720; 0.447215;
    -0.425323; 0.309011; 0.850654;
    -0.262869; 0.809012; 0.525738;
    0.688189; 0.499997; 0.525736;
    0.525730; 0.000000; 0.850652;
    0.894425; 0.000000; 0.447215;
    0.525730; 0.000000; 0.850652;
    0.688189; 0.499997; 0.525736;
    0.162456; 0.499995; 0.850654;
    0.162456; 0.499995; 0.850654;
    0.000000; 0.000000; 1.000000;
    0.525730; 0.000000; 0.850652;
    0.276385; 0.850640; 0.447215;
    0.162456; 0.499995; 0.850654;
    0.688189; 0.499997; 0.525736;
  |]


end
module Renderer = struct


  let init_canvas canvas_id =
    let canvas =
      Opt.get
        (Opt.bind ( Dom_html.document##getElementById(string canvas_id) )
           Dom_html.CoerceTo.canvas)
        (fun () -> error "can't find canvas element %s" canvas_id) in
    let gl =
      Opt.get (try WebGL.getContext canvas with e -> null)
        (fun () -> alert "can't initialise webgl context") in
    canvas, gl

  let load_shader (gl:WebGL.renderingContext t) shader text =
    gl##shaderSource(shader,text);
    gl##compileShader(shader);
    if not (to_bool gl##getShaderParameter(shader, gl##_COMPILE_STATUS_))
    then error "An error occurred compiling the shaders: \n%s\n%s"
        (to_string text)
        (to_string gl##getShaderInfoLog(shader))

  let create_program (gl:WebGL.renderingContext t) vert_src frag_src =
    let vertexShader = gl##createShader(gl##_VERTEX_SHADER_) in
    let fragmentShader = gl##createShader(gl##_FRAGMENT_SHADER_) in
    load_shader gl vertexShader vert_src;
    load_shader gl fragmentShader frag_src;
    let prog = gl##createProgram() in
    gl##attachShader(prog,vertexShader);
    gl##attachShader(prog,fragmentShader);
    gl##linkProgram(prog);
    if not (to_bool gl##getProgramParameter(prog, gl##_LINK_STATUS_))
    then error "Unable to link the shader program.";
    prog

  let get_source src_id =
    let script = Opt.get
        (Opt.bind ( Dom_html.document##getElementById(string src_id) )
           Dom_html.CoerceTo.script)
        (fun () -> error "can't find script element %s" src_id) in
    script##text

  let float32array a =
    let array = jsnew Typed_array.float32Array(Array.length a) in
    Array.iteri (fun i v -> Typed_array.set array i v) a;
    array
  let float32array_concat_map al ~f =
    let length = List.fold_left al ~init:0 ~f:(fun p a -> p + Array.length (f a)) in
    let array = jsnew Typed_array.float32Array(length) in
    let idx = ref 0 in
    List.iter al ~f:(fun a ->
        Array.iter (f a) ~f:(fun v ->
            Typed_array.set array !idx v;
            incr idx)
      );
    array

  let int16array a =
    let array = jsnew Typed_array.int16Array(Array.length a) in
    Array.iteri (fun i v -> Typed_array.set array i v) a;
    array

  module Proj3D = struct
    type t = float array
    let scale x y z =
      [| x;  0.; 0.; 0.;
         0.; y ; 0.; 0.;
         0.; 0.; z ; 0.;
         0.; 0.; 0.; 1.; |]

    let translate x y z =
      [| 1.; 0.; 0.; 0.;
         0.; 1.; 0.; 0.;
         0.; 0.; 1.; 0.;
         x ; y ; z ; 1.; |]

    let rotate_x t =
      [| 1.; 0.;      0.;    0.;
         0.; cos t;   sin t; 0.;
         0.; -.sin t; cos t; 0.;
         0.; 0.;      0.;    1.; |]

    let rotate_y t =
      [| cos t; 0.; -.sin t; 0.;
         0.;    1.; 0.;      0.;
         sin t; 0.; cos t;   0.;
         0.;    0.; 0.;    1.; |]

    let c i j = i * 4 + j
    let o i = i/4, i mod 4

    let mult m1 m2 =
      let v p =
        let i,j = o p in
        m1.(c i 0) *. m2.(c 0 j)
        +. m1.(c i 1) *. m2.(c 1 j)
        +. m1.(c i 2) *. m2.(c 2 j)
        +. m1.(c i 3) *. m2.(c 3 j) in
      Array.init 16 v

    let array m = float32array m

  end

  type scene_object =
    | Sphere of (float array * float * float array) (* position, scale, color *)

  type scene = scene_object list

  let white = [| 1.; 1.; 1. |]
  let sphere ?(color=white) ~position ~scale () = Sphere (position, scale, color)

  type renderer = {
    gl: WebGL.renderingContext Js.t;
    prog: WebGL.program Js.t;
    mutable render: unit -> unit
  }

  let start () =
    let fps_text = Dom_html.document##createTextNode (Js.string "loading") in
    Opt.iter
      (Opt.bind ( Dom_html.document##getElementById(string "fps") )
         Dom_html.CoerceTo.element)
      (fun span -> Dom.appendChild span fps_text);

    let canvas, gl = init_canvas "canvas" in
    let prog = create_program gl
        (get_source "vertex-shader")
        (get_source "fragment-shader") in
    gl##useProgram(prog);

    check_error gl;
    debug "program loaded";

    gl##enable(gl##_DEPTH_TEST_);
    gl##depthFunc(gl##_LESS);

    let proj_loc = gl##getUniformLocation(prog, string "u_proj") in
    let lightPos_loc = gl##getUniformLocation(prog, string "u_lightPos") in
    let ambientLight_loc = gl##getUniformLocation(prog, string "u_ambientLight") in

    let lightPos = float32array [| 3.; 0.; -. 1. |] in
    let ambientLight = float32array [| 0.1; 0.1; 0.1 |] in

    gl##uniform3fv_typed(lightPos_loc, lightPos);
    gl##uniform3fv_typed(ambientLight_loc, ambientLight);



    let mat =
      (* let pi = 4. *. (atan 1.) in *)
      Proj3D.(
        mult
          (* (rotate_x (pi/.2.)) *)
          (rotate_x (0.))
          (mult
             (* (scale 0.8 0.8 0.8) *)
             (scale 1. 1. 1.)
             (translate (0.) (0.) 0.))) in

    check_error gl;
    debug "ready";

    let get_time () = to_float ((jsnew date_now ())##getTime()) in
    let last_draw = ref (get_time ()) in
    let draw_times = Queue.create () in
    let renderer = { gl; render = (fun () -> ()); prog} in

    let interaction_mat = ref mat in
    let interaction_mode = ref (`turning 0.1) in
    canvas##ondblclick <- Dom_html.handler (fun click_ev ->
        begin match !interaction_mode with
        | `turning _ | `dragging _ ->
          (* we use the double-click to stop everything (panic button) *)
          interaction_mode := `stopped;
        | `stopped ->
          interaction_mode := `turning 0.1;
        end;
        Js._true
      );
    (* Disable drag-and-drop on the canvas: *)
    canvas##ondragstart <- Dom_html.handler (fun _ -> Js._false);
    canvas##ondrop <- Dom_html.handler (fun _ -> Js._false);

    canvas##onmousedown <- Dom_html.handler (fun click_ev ->
        interaction_mode := `dragging (!interaction_mode,
                                       click_ev##clientX,
                                       click_ev##clientY,
                                       click_ev##clientX,
                                       click_ev##clientY);
        Js._true
      );
    canvas##onmouseup <- Dom_html.handler (fun click_ev ->
        begin match !interaction_mode with
        | `dragging (previous, _, _, _, _) ->
          interaction_mode := previous
        | _ -> ()
        end;
        Js._true);
    canvas##onmousemove <- Dom_html.handler (fun click_ev ->
        begin match !interaction_mode with
        | `dragging (previous, px, py, nx, ny) ->
          interaction_mode := `dragging (previous,
                                         px, py,
                                         click_ev##clientX,
                                         click_ev##clientY);
        | _ -> ()
        end;
        Js._true);

    let rec f () =
      begin match !interaction_mode with
      | `turning f ->
        interaction_mat := Proj3D.mult !interaction_mat (Proj3D.rotate_y (f));
      | `dragging (previous, px, py, nx, ny) ->
        let rot = Proj3D.(
            mult
              (rotate_x (float_of_int (py - ny) *. 0.01))
              (rotate_y (float_of_int (px - nx) *. 0.01))
          ) in
        debug "rot y : %f" (float_of_int (ny - py) *. 0.01);
        debug "rot x : %f" (float_of_int (nx - px) *. 0.01);
        interaction_mat := Proj3D.mult !interaction_mat rot;
        interaction_mode := `dragging (previous,
                                       nx, ny,
                                       nx, ny);
      | _ -> ()
      end;
      gl##uniformMatrix4fv_typed(proj_loc, _false,
                                 Proj3D.array !interaction_mat);

      gl##clear(gl##_DEPTH_BUFFER_BIT_ lor gl##_COLOR_BUFFER_BIT_);
      renderer.render ();
      check_error gl;

      let now = get_time () in
      Queue.push (now -. !last_draw) draw_times;
      last_draw := now;
      if Queue.length draw_times > 50 then ignore (Queue.pop draw_times);
      let fps = (1. /. ( Queue.fold (+.) 0. draw_times ))
                *. (Pervasives.float (Queue.length draw_times))
                *. 1000. in
      fps_text##data <- string (Printf.sprintf "%.1f" fps);
      Lwt_js.sleep 0.1 >>= f
    in
    Lwt.ignore_result (f ());
    return renderer

  let load_scene ~renderer scene =
    let arrays =
      List.map scene ~f:(function
        | Sphere (position, scale, color) ->
          let scaled_and_translated =
            Array.mapi Data.sphere_vertices ~f:(fun i coord ->
                let kind_of_coord = i mod 3 in
                (coord *. scale) +. position.(kind_of_coord)) in
          Lwt_js.yield () |> Lwt.ignore_result;
          let color_array =
            Array.mapi Data.sphere_vertices ~f:(fun i _ ->
                let kind_of_coord = i mod 3 in
                color.(kind_of_coord)) in
          scaled_and_translated, Data.sphere_vertices, color_array)
    in
    debug "arrays crated";
    let pos = float32array_concat_map arrays ~f:(fun (x, _, _) -> x) in
    debug "pos crated";
    let norm = float32array_concat_map arrays  ~f:(fun (_, x, _) -> x) in
    let color = float32array_concat_map arrays  ~f:(fun (_, _, x) -> x) in

    let gl = renderer.gl in
    let prog = renderer.prog in

    let pos_attr = gl##getAttribLocation(prog, string "a_position") in
    gl##enableVertexAttribArray(pos_attr);
    let array_buffer = gl##createBuffer() in
    gl##bindBuffer(gl##_ARRAY_BUFFER_,array_buffer);
    gl##bufferData(gl##_ARRAY_BUFFER_,pos,gl##_STATIC_DRAW_);
    gl##vertexAttribPointer(pos_attr, 3, gl##_FLOAT, _false, 0, 0);

    let norm_attr = gl##getAttribLocation(prog, string "a_normal") in
    gl##enableVertexAttribArray(norm_attr);
    let norm_buffer = gl##createBuffer() in
    gl##bindBuffer(gl##_ARRAY_BUFFER_,norm_buffer);
    gl##bufferData(gl##_ARRAY_BUFFER_,norm,gl##_STATIC_DRAW_);
    gl##vertexAttribPointer(norm_attr, 3, gl##_FLOAT, _false, 0, 0);

    let color_attr = gl##getAttribLocation(prog, string "a_color") in
    gl##enableVertexAttribArray(color_attr);
    let color_buffer = gl##createBuffer() in
    gl##bindBuffer(gl##_ARRAY_BUFFER_,color_buffer);
    gl##bufferData(gl##_ARRAY_BUFFER_,color,gl##_STATIC_DRAW_);
    gl##vertexAttribPointer(color_attr, 3, gl##_FLOAT, _false, 0, 0);

    renderer.render <- (fun () ->
        gl##drawArrays(gl##_TRIANGLES, 0, pos##length / 3);
      );
    return ()

end

module PDB = struct


  let array_iter f a =
    let rec aux i =
      match Optdef.to_option (array_get a i) with
      | None -> ()
      | Some s -> f s; aux (i+1) in
    aux 0

  type atom = string * float array
  type molecule = atom list

  let read_model content =
    debug "read_model";
    let a = str_array ((string content)##split(string "\n")) in
    (Unsafe.coerce Dom_html.window)##arr <- a;
    let atoms = ref [] in
    let max = [| -. infinity; -. infinity; -. infinity |] in
    let min = [|    infinity;    infinity;    infinity |] in
    array_iter (fun s ->
        let tokens =
          str_array (s##split_regExp(jsnew regExp(string "[ \t]+"))) in
        let get_string_from_array a idx =
          to_string (Optdef.get (array_get a idx)
                       (fun () -> error "array access at %d" idx)) in
        let kind = get_string_from_array tokens 0 in
        if kind = "ATOM"
        then (
          let element = get_string_from_array tokens 2 in
          let x = get_string_from_array tokens 6 |> float_of_string in
          let y = get_string_from_array tokens 7 |> float_of_string in
          let z = get_string_from_array tokens 8 |> float_of_string in
          if x < min.(0) then min.(0) <- x;
          if y < min.(1) then min.(1) <- y;
          if z < min.(2) then min.(2) <- z;
          if x > max.(0) then max.(0) <- x;
          if y > max.(1) then max.(1) <- y;
          if z > max.(2) then max.(2) <- z;
          (* debug "Atom: %S (%s, %s, %s)" element x y z; *)
          atoms := (element, [| x;  y;  z |]) :: !atoms;
        );
        Lwt_js.yield () |> Lwt.ignore_result;
      ) a;
    let molecule =
      ("Z", min) :: ("Z", max) :: List.rev !atoms in
    return (molecule, min, max)

  let to_scene (molecule, aabb_min, aabb_max) =
    let max_coord =
      max (max (aabb_max.(0) -. aabb_min.(0)) (aabb_max.(1) -. aabb_min.(1)))
        (aabb_max.(2) -. aabb_min.(2)) in
    debug "AABB: min: %f, %f, %f  |  max: %f, %f, %f | max_coord: %f"
      aabb_min.(0) aabb_min.(1) aabb_min.(2)
      aabb_max.(0) aabb_max.(1) aabb_max.(2) max_coord;
    let scale = 0.2 in
    let red = [| 1.; 0.; 0. |] in
    (
         Renderer.sphere ~position:[| -1.; -1.; -1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[|  1.;  1.;  1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[| -1.;  1.;  1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[|  1.; -1.;  1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[|  1.; -1.; -1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[|  1.;  1.; -1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[| -1.;  1.; -1. |] ~scale ~color:red ()
      :: Renderer.sphere ~position:[| -1.; -1.;  1. |] ~scale ~color:red ()
      :: List.map molecule ~f:(fun (el, position) ->
          Lwt_js.yield () |> Lwt.ignore_result;
          let scale =
            (try (float_of_int (int_of_char el.[0])) /. 3000. with _ -> 1.) in
          let color =
            match try el.[0] with _ -> 'A' with
            | 'A' -> [| 1.; 1.; 0.; |]
            | 'C' -> [| 0.; 0.; 0.; |]
            | 'N' -> [| 0.; 0.; 1.; |]
            | 'O' -> [| 1.; 0.; 0.; |]
            | 'P' -> [| 0.; 1.; 0.; |]
            | 'S' -> [| 0.; 1.; 1.; |]
            | _ -> debug "white! %S" el; [| 1.; 1.; 1.; |] in
          let position = [|
            (1.4 *. (position.(0) -. aabb_min.(0)) /. max_coord) -. 0.5;
            (1.4 *. (position.(1) -. aabb_min.(1)) /. max_coord) -. 0.5;
            (1.4 *. (position.(2) -. aabb_min.(2)) /. max_coord) -. 0.5;
          |] in
          Renderer.sphere ~color ~position ~scale ()))

end

let fetch_model s =
  XmlHttpRequest.perform_raw_url s
  >>= fun frame ->
  PDB.read_model frame.XmlHttpRequest.content


let default_model =  "http://www.pdb.org/pdb/files/4IS8.pdb"

let setup_download_box renderer =
  let interaction_div =
    Opt.get (Dom_html.document##getElementById(string "interaction"))
      (fun () -> failwith "no interaction div")  in
  let input_url =
    Dom_html.createInput ~_type:(string "text") Dom_html.document in
  let load_scene () =
    Lwt.ignore_result begin
      Renderer.load_scene ~renderer []
      >>= fun () ->
      fetch_model (to_string input_url##value)
      >|= PDB.to_scene
      >>= fun scene ->
      Renderer.load_scene ~renderer scene
    end;
  in
  input_url##value <- string default_model;
  input_url##size <- 50;
  input_url##onkeyup <- Dom_html.handler begin fun ev ->
      if ev##keyCode = 13 then (
        debug "Go: %s" (to_string input_url##value);
        load_scene ();
      );
      Js._true
    end;
  let go_button = Dom_html.createButton Dom_html.document in
  go_button##innerHTML <- string "Go !";
  go_button##style##color <- string "#f00";
  go_button##onclick <- Dom_html.handler begin fun _ ->
      debug "Go: %s" (to_string input_url##value);
      load_scene ();
      Js._true
    end;
  Dom.appendChild interaction_div input_url;
  Dom.appendChild interaction_div go_button;
  return ()

let go _ =
  ignore (
    catch begin fun () ->
      debug "START !";
      Renderer.start ()
      >>= fun renderer ->
      setup_download_box renderer
      >>= fun () ->
      fetch_model default_model
      >|= PDB.to_scene
      >>= fun scene ->
      Renderer.load_scene ~renderer scene
    end
      (fun exn -> error "uncaught exception: %s" (Printexc.to_string exn)));
  _true

let _ = Dom_html.window##onload <- Dom_html.handler go
